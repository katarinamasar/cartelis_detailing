$(document).ready(function(){

    // menu 

   

    $('.mobileMenu').click(function() {
        $(this).toggleClass('change');
        $('.nav-wrapper').toggle();
    });

    var hash_ = '';

    var pageYoffsetPlus = 500 < $(window).width() < 950 ? 77 : 120;

    if ( $(window).width() > 1000 ) {
        pageYoffsetPlus = 125;
    }
    else if ( $(window).width() > 2000 ) {
        pageYoffsetPlus = 130;
    }

    $(document).bind('scroll',function(e){


        $('section').each(function(){
            if (
                $(this).offset().top < window.pageYOffset + pageYoffsetPlus + 5  //begins before top
                && $(this).offset().top + $(this).height() > window.pageYOffset + pageYoffsetPlus  //+ xy allows you to change hash before it hits the top border
            ) {
                hash_ = ($(this).attr('id'));
                let active = $(document).find($('a[href="#' + hash_ + '"]'));

                active.parent().addClass('active').siblings().removeClass('active');
                
                if(hash_ === 'cennik' || hash_ === 'detailing-interieru' || hash_ === 'korekcia-laku' || hash_ === 'sluzby') {
                    $('.open-sub-nav').addClass('active').siblings().removeClass('active');
                }
            }

        });
    });



    // services sub nav

    var subNav = $('.sub-nav-services');
    var feynlab = $('.feynlab');

    
    if (window.location.href.indexOf("feynlab") > -1) {
        $(".header--ul-left .header--nav-link").click(function() {
            if( !$(this).is('.open-sub-nav') ) {
                let hashX = $(this).children().attr('href');
                window.location.href = "/" + hashX;
            }
        })
    }

    $(".header--ul-left .header--nav-link").click(function(event){
        event.preventDefault();

        if( !$(this).is('.open-sub-nav') ) {

            feynlab.removeClass('active');
            
            var to_id = $(this).children().attr('href');
    
            if(to_id) {
                $('html, body').stop().animate({
                    scrollTop: $(to_id).offset().top - pageYoffsetPlus
                }, 1000);
    
                $('.mobileMenu').toggleClass('change');
                $('.nav-wrapper').toggle();
                window.location.hash = to_id;
            }

            subNav.slideUp();
        }
    });

    $('.open-sub-nav').click(function() {
        subNav.children().removeClass('active');
        
        $(this).toggleClass('opened');

        subNav.slideToggle();
    });

    subNav.click(function() {
        $('.open-sub-nav').addClass('active').siblings().removeClass('active');
    });

    $(document).click(function(e) {
        let clickedElement = e.target.className;
        if ( !clickedElement.includes('sub-nav') ) {
            if ($('.open-sub-nav').hasClass('opened')) {
                subNav.slideUp();
                $('.open-sub-nav').removeClass('opened');
            }
        }
    });
    

    // $('.header--mobile .sub-nav-services').click(function() {
    //     $(this).toggleClass('.hidden .flex');
    // });


    

    // gallery slick slider

    
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        // fade: true,
        asNavFor: '.slider-nav'
      });
      $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        focusOnSelect: true, 
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
      });
});